﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace firstProject
{
    class firstProject
    {
        static void Main(string[] args)
        {
            int input = -1;

            while (input != 0)
            {
                displayOptions();
                Console.Write("\nEnter choice: ");
                input = int.Parse(Console.ReadLine());

                while (input < 0 || input > 6)
                {
                    Console.WriteLine("Invalid input, choose from 0 - 6");
                    Console.Write("Enter choice: ");
                    input = int.Parse(Console.ReadLine());
                }

                switch (input)
                {
                    case 1:

                        Calculator c = new Calculator();
                        c.getChoice();
                        break;

                    case 2:

                        WordCount w = new WordCount();
                        w.getFile();
                        break;

                    case 3:

                        WriteFile wf = new WriteFile();
                        wf.checkInput();
                        break;

                    case 4:

                        DisplayFile df = new DisplayFile();
                        df.display();
                        break;



                }
            }
        }

        static void displayOptions()
        {
            Console.WriteLine("Options: \n\n\t0)Exit \n\t1) Calculator \n\t2) Word Count \n\t3) Write/Append File" +
                "\n\t4) View File");
        }
    }

    class Calculator
    {
        double a;
        double b;
        String op;
        String ops = "+-*/%";

        public void getChoice()
        {

            while (true)
            {

                Console.WriteLine("\n\t\tCalculator Application");

                Console.Write("Value 1 = ");
                a = double.Parse(Console.ReadLine());

                Console.Write("Operator(+, -, *, /, %) = ");
                op = Console.ReadLine();

                while (!ops.Contains(op))
                {
                    Console.WriteLine("Invalid operator");
                    Console.Write("Enter operator: ");
                    op = Console.ReadLine();
                }

                Console.Write("Value 2 = ");
                b = double.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("{0} {1} {2} = ", a, op, b);

                switch (op)
                {
                    case "+":

                        Console.WriteLine(add());
                        break;

                    case "-":

                        Console.WriteLine(subtract());
                        break;

                    case "*":

                        Console.WriteLine(multiply());
                        break;

                    case "/":

                        Console.WriteLine(divide());
                        break;

                    case "%":

                        Console.WriteLine(mod());
                        break;

                }
                Console.WriteLine("\nExit Calculator? (y/n)");
                String z = Console.ReadLine();
                z = z.ToLower();
                if (z.Equals("y"))
                {
                    break;
                }
            }
        }

        public double add()
        {
            return a + b;

        }

        public double multiply()
        {
            return a * b;
        }

        public double divide()
        {
            return a / b;
        }

        public double subtract()
        {
            return a - b;
        }

        public double mod()
        {
            return a % b;
        }
    }

    class WordCount
    {
        public void getFile()
        {

            while (true)
            {
                int count = 0;
                Console.Write("\nEnter file to be read: ");
                String file = Console.ReadLine();
                try
                {

                    using (StreamReader sr = new StreamReader(file))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {

                            String[] words = line.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);

                            foreach (string word in words)
                            {
                                count++;
                            }
                            Console.WriteLine("\nWord count = " + count);


                        }

                    }
                }
                catch (System.IO.IOException)
                {
                    Console.WriteLine("IO Exception");

                }
                Console.Write("\nContinue Word Count? (y/n)");
                String stri = Console.ReadLine();

                if (stri.Equals("n"))
                {
                    break;
                }

            }
        }

    }


    class WriteFile
    {

        public void checkInput()
        {
            //check if the file already exists

            while (true)
            {
                Console.Write("\nEnter the file name: ");
                String file = Console.ReadLine();

                if (File.Exists(file))
                {
                    append(file);
                }
                else
                {
                    Console.Write("\nFile does not exist, create file : " + file + " ?(y/n)");
                    String s = Console.ReadLine();
                    if (s.Equals("y"))
                    {
                        create(file);
                    }
                }

                Console.Write("\nContinue writing? (y/n) : ");
                String z = Console.ReadLine();
                if (z.Equals("n"))
                {
                    break;
                }

            }


        }

        public void append(String file)
        {
            Console.Write("\nEnter text: ");
            String text = Console.ReadLine();

            using (StreamWriter sw = File.AppendText(file))
            {
                sw.WriteLine(text);
            }
        }

        public void create(String file)
        {
            Console.Write("\nEnter text: ");
            String text = Console.ReadLine();

            using (StreamWriter sw = File.CreateText(file))
            {
                sw.WriteLine(text);
            }

        }

    }

    class DisplayFile
    {
        public void display()
        {
            Console.Write("\nEnter file name: ");
            String file = Console.ReadLine();
            Console.WriteLine();

            while (true)
            {

                if (File.Exists(file))
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            Console.WriteLine(line);
                        }
                    }


                }
                else
                {
                    Console.WriteLine("\nFile does not exist!");
                }

                Console.Write("\nRead another file? (y/n) : ");
                String s = Console.ReadLine();
                if (s.Equals("n"))
                {
                    break;
                }

            }


        }
    }

}



